import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';
import {Images} from "../imports/api/collections";

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.loading=new ReactiveVar(false)
});

Template.hello.helpers({
  loading(){
    return Template.instance().loading.get()
  }
});

Template.hello.events({
  'click #upload'(event, instance) {
    // increment the counter when button is clicked
    let file=document.getElementById('myFile').files[0]

    const upload = Images.insert({
      file,
      chunkSize: 'dynamic'
    }, false);

    upload.on('start', function () {
      instance.loading.set(this);
    });

    upload.on('end', function (error, fileObj) {
      if (error) {
        alert(`Error during upload: ${error}`);
      } else {
        alert(`File "${fileObj.name}" successfully uploaded`);
      }
      instance.loading.set(false);
    });

    upload.start();
  },
});
